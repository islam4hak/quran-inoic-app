import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { QuranService } from '../providers/quran.service';

@Component({
  selector: 'app-bookmarked',
  templateUrl: './bookmarked.page.html',
  styleUrls: ['./bookmarked.page.scss'],
})
export class BookmarkedPage implements OnInit {
  ayats = JSON.parse(localStorage.getItem('bookmarkedAyat'))
  constructor(public nav:NavController , public quraan:QuranService) { }

  ngOnInit() {
  }

  goToSurra(item){
    this.quraan.autoPlay = item.number;
    this.nav.navigateForward('play/'+item.SourrahId)
  }

}
