import { Component, OnInit } from '@angular/core';
import { QuranService } from '../providers/quran.service';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import {Howl, Howler} from 'howler';
import { NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-play',
  templateUrl: './play.page.html',
  styleUrls: ['./play.page.scss'],
})
export class PlayPage implements OnInit {
  id ;
  data : any;
  thePlayList;
  player : Howl = null;
  activeTrack = null
  isPlaying = false;
  listOfReaders = [];
  defualtReader = "";
  constructor(private QuranService:QuranService,
    private route: ActivatedRoute,
    private router: Router,
    private navCtrl: NavController,
    public toastCtrl: ToastController
    ) {
      // this.id = this.navParams.get('id');
      const url = window.location.href;
      console.log(url);
      const last = url.split("/")

      this.id = last[last.length-1]; // "tutorial"
     }

  ngOnInit() {
    console.log("id: "+this.id)

    this.QuranService.getSoura(this.id).subscribe((res)=>{
      this.data = res.data;
      this.thePlayList = res.data.ayahs;
      console.log(this.data);
      this.QuranService.dismmisLoader()
      if(this.QuranService.autoPlay != false){
        this.thePlayList.forEach(element => {
          if(element.number == this.QuranService.autoPlay ){
            this.start(element);
          }
        });
      }

      // get all list of readers 
      this.QuranService.getListOfReaders().subscribe((res)=>{
        this.listOfReaders = res.data;
        this.QuranService.dismmisLoader()
      });
   
      // console.log(res.data.surahs);
    })
  }

  showToastMsg() {
    this.toastCtrl.create({
      message: 'تم اضافة علامة الترقيم !',
      duration: 1800
    }).then((toastRes) => {
      console.log(toastRes);
      toastRes.present();
    });
  }  

  start(track){
    if(this.player !== null){
      this.player.stop()

    }

    console.log('start')
    this.player = new Howl({
      src: [track.audio],
      html5:true,
      onplay: ()=>{
        if(this.player.ispla)
        console.log('on play')
        this.isPlaying = true;
        this.activeTrack = track;
      },
      onend: ()=>{
        console.log('on end')
        this.next();
      }
    })
    this.player.play();
  }

  togglePlayer(pause){
    this.isPlaying = !pause;
    if(pause){
      this.player.pause();
    } else{
      this.player.play();
    }
  }
  prev(){
    let index = this.thePlayList.indexOf(this.activeTrack);
    console.log(index);
    if(index >0 ){
      this.start(this.thePlayList[index -1] );
    }else{
      this.start(this.thePlayList[this.thePlayList.length - 1 ])
    }
  }
  next(){
    let index = this.thePlayList.indexOf(this.activeTrack);
    console.log(index);
    if(index != this.thePlayList.length - 1   ){
      this.start(this.thePlayList[index + 1] );
    }else{
      this.start(this.thePlayList[0])
    }
  }
  goBack(){
    this.navCtrl.back();
  }

  ionViewWillLeave (){
    console.log('view leave');
    this.player.stop();
    this.thePlayList = [] ; 
    this.player = null;

  }

  changeReader(event){
    // console.log('uska')
    // console.log(event.detail.value);
    this.QuranService.reader = event.detail.value;
    console.log("id: "+this.id)
    let activeTrack = this.activeTrack;
    console.log(activeTrack);
    this.QuranService.autoPlay = activeTrack.number;
    this.QuranService.getSoura(this.id).subscribe((res)=>{
      this.data = res.data;
      this.thePlayList = res.data.ayahs;
      console.log(this.data);
      this.QuranService.dismmisLoader()
      // this.start(activeTrack);
      if(this.QuranService.autoPlay != false){
        this.thePlayList.forEach(element => {
          if(element.number == this.QuranService.autoPlay ){
            this.start(element);
          }
        });
      }

      // get all list of readers 
      this.QuranService.getListOfReaders().subscribe((res)=>{
        this.listOfReaders = res.data;
        this.QuranService.dismmisLoader()
      });
   
      // console.log(res.data.surahs);
    })
  }

  bookmark(item){
    console.log(item);
    item.SourrahId = this.id;
    let BookmarkedAyat =JSON.parse( localStorage.getItem('bookmarkedAyat'));
    console.log(BookmarkedAyat);
    if(BookmarkedAyat === null){
      BookmarkedAyat = [];
      BookmarkedAyat.push(item)
    }else{
      if (!BookmarkedAyat.find(o => o.number === item.number ))
      BookmarkedAyat.push(item)
    }
   localStorage.setItem('bookmarkedAyat',JSON.stringify(BookmarkedAyat));
   this.showToastMsg();

  }



}

