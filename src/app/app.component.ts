import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'مصحف المدينة المنورة',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'فهرس المصحف',
      url: '/list',
      icon: 'list'
    },
    {
      title: 'استمع الى القرأن',
      url: '/listen',
      icon: 'journal'
    },
    {
      title: 'الترقيمات المحفوظة كتابة ',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'الترقيمات المحفوظة (استماع) ',
      url: '/bookmarked',
      icon: 'book'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
