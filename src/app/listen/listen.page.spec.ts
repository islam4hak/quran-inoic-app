import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListenPage } from './listen.page';

describe('ListenPage', () => {
  let component: ListenPage;
  let fixture: ComponentFixture<ListenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListenPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
