import { Component, OnInit } from '@angular/core';
import { QuranService } from '../providers/quran.service';

@Component({
  selector: 'app-listen',
  templateUrl: './listen.page.html',
  styleUrls: ['./listen.page.scss'],
})
export class ListenPage implements OnInit {
  quraan = [] ;
  constructor(public QuranService:QuranService) { }

  ngOnInit() {
    this.QuranService.getListOfAyat().subscribe((res)=>{
      this.quraan = res.data;
      this.QuranService.dismmisLoader()
      console.log(res.data);
    })
    
    // QuranService.get.subscribe(response => {
    // console.log(response);
    // this.studentsData = response;
  }

  test(){
    this.QuranService.getListOfAyat().subscribe((res)=>{

    })
  }


}
