import { Component, ViewChildren, OnInit } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { ViewChild } from '@angular/core'
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

// @Component({
//   selector: 'app-home',
//   template: `
//     <ion-slides pager="true" [options]="slideOpts">
//       <ion-slide>
//         <h1>Slide 1</h1>
//       </ion-slide>
//       <ion-slide>
//         <h1>Slide 2</h1>
//       </ion-slide>
//       <ion-slide>
//         <h1>Slide 3</h1>
//       </ion-slide>
//     </ion-slides>
//   `
// })

export class HomePage  {

  @ViewChild(IonSlides, { static: false }) slides: IonSlides;
  private koko = 1;
  public activePage = 1;

  // Optional parameters to pass to the swiper instance. See http://idangero.us/swiper/api/ for valid options.
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  currentIndex:Number = 0;


  constructor(public toastCtrl: ToastController) {
  }
  showToastMsg() {
    this.toastCtrl.create({
      message: 'تم اضافة علامة الترقيم !',
      duration: 1800
    }).then((toastRes) => {
      console.log(toastRes);
      toastRes.present();
    });
  }

  ionViewDidLoad(){
    //alert( this.koko);
  }

  getIndex(event){
    this.slides.getActiveIndex().then(
      (index)=>{
        //  localStorage.setItem('QuranPage',''+index);
        // this.showToastMsg();
        this.activePage = index;
        this.activePage = index;
     });
  }

  ionViewDidEnter(){
    this.koko = parseInt(localStorage.getItem("QuranPage"));
    //alert(this.koko );
    this.slides.slideTo(this.koko);
  }


  bookmarkPage(){
    this.slides.getActiveIndex().then(
      (index)=>{
         localStorage.setItem('QuranPage',''+index);
         this.koko = index;
        this.showToastMsg();
     });
  }




}
